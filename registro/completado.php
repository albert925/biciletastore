<?php
	include '../config.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<meta name="description" content="" />
	<title>Coyote registro usuarios</title>
	<link rel="icon" href="../imagenes/icono.png" />
	<link rel="image_src" href="../imagenes/icono.png" />
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/default/default.css" />
	<link rel="stylesheet" href="../css/nivo_slider.css" />
	<script src="../js/jquery_2_1_1.js"></script>
	<script src="../js/scrpag.js"></script>
	<script type="text/javascript">
		setTimeout(direcionar,3000);
		function direcionar () {
			window.location.href="../";
		}
	</script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../imagenes/logo.png" alt="Logo" />
			</a>
		</figure>
		<nav>
			<a id="inus" href="../registro">
				<figure></figure>
			</a>
			<a href="../carrito.php">
				<div id="caritod">
					<span class="icon-bicil_carr2"></span>
					<span id="decar">carrito 0</span>
				</div>
			</a>
		</nav>
	</header>
	<nav id="mnP">
		<ul>
			<li><a href="../">Inicio</a></li>
			<?php
				$tiposP="SELECT * from tipo_producto order by id_tipo asc";
				$sql_tipoP=mysql_query($tiposP,$conexion) or die (mysql_error());
				while ($sl=mysql_fetch_array($sql_tipoP)) {
					$idtp=$sl['id_tipo'];
					$nmtp=$sl['nam_tipo'];
			?>
			<li>
				<a href="../producto/ind2x.php?tp=<?php echo $idtp ?>" data-mn="<?php echo $idtp ?>"><?php echo "$nmtp"; ?></a>
			</li>
			<?php
				}
			?>
			<?php
				if ($idus!="0") {
			?>
			<a href="factura" data-mn="0">Historial compras</a>
			<?php
				}
			?>
			<li><div id="busMs"><span class="icon-search"></span></div></li>
		</ul>
		<div id="btnmovil"><span class="icon-menu"></span></div>
	</nav>
	<nav id="mnB">
		<a href="../">Inicio</a>
		<?php
			$BtiposPb="SELECT * from tipo_producto order by id_tipo asc";
			$bsql_tipoPB=mysql_query($BtiposPb,$conexion) or die (mysql_error());
			while ($slB=mysql_fetch_array($bsql_tipoPB)) {
				$Bidtp=$slB['id_tipo'];
				$Bnmtp=$slB['nam_tipo'];
		?>
		<a href="../producto/ind2x.php?tp=<?php echo $Bidtp ?>"><?php echo "$Bnmtp"; ?></a>
		<?php
			}
		?>
		<a href="../contacto">Contacto</a>
	</nav>
	<aside id="busqueda">
		<article>
			<input type="search" id="busplpd" />
		</article>
		<div id="resultadoBs"></div>
	</aside>
	<section class="sectionCol">
		<h1>Registro Completado</h1>
		<article id="automargen" class="flexcjA">
			<p>
				Registro completado, le ha llegado un link de activación a su respectivo correo registrado.
			</p>
		</article>
	</section>
	<footer>
		<article class="flexfoot">
			<article class="flexxdos">
				<article>
					<a href="../">Inicio</a>
					<a class="sele" href="../nosotros">Nosotros</a>
					<a href="../contacto">Contacto</a>
				</article>
				<article>
					<div><b>Dirección:</b> calle 32A # 34 – 541 local 5 Av. Sincelejito.</div>
					<div><b>Teléfono:</b> (5) 275 10 65</div>
					<div><b>Correo:</b> servicioalcliente@bicicletascoyote.co</div>
				</article>
			</article>
			<article id="redes">
				<a href="" target="_blank"><span class="icon-facebook2"></span></a>
				<a href="" target="_blank"><span class="icon-instagram"></span></a>
				<a href="" target="_blank"><span class="icon-twitter"></span></a>
			</article>
		</article>
		<article id="fotfin">
			CONAXPORT © 2015 &nbsp;&nbsp;todo los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>