-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-12-2015 a las 14:57:34
-- Versión del servidor: 5.5.46-cll
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `conaxpor_coyote`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `us_adm` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cor_adm` varchar(355) COLLATE utf8_spanish_ci NOT NULL,
  `pass_adm` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `us_adm`, `cor_adm`, `pass_adm`) VALUES
(1, 'admin', 'admin@dominio.com', 'coyote123.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `id_ciudad` int(11) NOT NULL AUTO_INCREMENT,
  `pais_id` int(11) DEFAULT NULL,
  `nam_ciudad` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `pais_id` (`pais_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `pais_id`, `nam_ciudad`) VALUES
(1, 52, 'Leticia'),
(2, 52, 'Medellin'),
(3, 52, 'Arauca'),
(4, 52, 'Barranquilla'),
(5, 52, 'Cartagena'),
(6, 52, 'Tunja'),
(7, 52, 'Manizales'),
(8, 52, 'Florencia'),
(9, 52, 'Yopal'),
(10, 52, 'Popayan'),
(11, 52, 'Valledupar'),
(12, 52, 'Quibdo'),
(13, 52, 'Monteria'),
(14, 52, 'Bogota'),
(15, 52, 'Inirida'),
(16, 52, 'San Jose del Guaviare'),
(17, 52, 'Neiva'),
(18, 52, 'Riohacha'),
(19, 52, 'Santa Marta'),
(20, 52, 'Villavicencio'),
(21, 52, 'Pasto'),
(22, 52, 'Cucuta'),
(23, 52, 'Mocoa'),
(24, 52, 'Armenia'),
(25, 52, 'Pereira'),
(26, 52, 'San Andres'),
(27, 52, 'Bucaramanga'),
(28, 52, 'Sincelejo'),
(29, 52, 'Ibague'),
(30, 52, 'Cali'),
(31, 52, 'Mitu'),
(32, 52, 'Puerto CarreÃ±o');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cl` int(11) NOT NULL AUTO_INCREMENT,
  `tp_id` int(11) DEFAULT NULL,
  `nam_cl` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_cl`),
  KEY `tp_id` (`tp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cl`, `tp_id`, `nam_cl`) VALUES
(6, NULL, 'Hombres'),
(7, NULL, 'Mujeres'),
(8, NULL, 'NiÃ±os');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE IF NOT EXISTS `colores` (
  `id_color` int(11) NOT NULL AUTO_INCREMENT,
  `nam_color` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id_color`, `nam_color`) VALUES
(1, 'verde'),
(3, 'azul');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `cod_f` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fec_f` datetime NOT NULL,
  `n_vent` decimal(10,0) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `talla_f` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `color_f` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cant_f` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `subt_f` decimal(10,0) NOT NULL,
  `total_f` decimal(10,0) NOT NULL,
  `estd_f` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_f`),
  KEY `producto_id` (`producto_id`,`usuario_id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galery`
--

CREATE TABLE IF NOT EXISTS `galery` (
  `id_gal` int(11) NOT NULL AUTO_INCREMENT,
  `rut_gal` varchar(450) COLLATE utf8_spanish_ci NOT NULL,
  `producto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_gal`),
  KEY `producto_id` (`producto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `galery`
--

INSERT INTO `galery` (`id_gal`, `rut_gal`, `producto_id`) VALUES
(8, 'imagenes/galery/photo_2015-11-25_11-24-14.jpg', NULL),
(9, 'imagenes/galery/bici4.jpg', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_pr`
--

CREATE TABLE IF NOT EXISTS `img_pr` (
  `id_img_p` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `ruta_pr` varchar(455) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_img_p`),
  KEY `producto_id` (`producto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `img_pr`
--

INSERT INTO `img_pr` (`id_img_p`, `producto_id`, `ruta_pr`) VALUES
(11, 15, 'imagenes/producto/00bc5a6eaf69047c76308c11da141793.jpg'),
(15, 17, 'imagenes/producto/Venge 2.png'),
(16, 18, 'imagenes/producto/SL 4.png'),
(17, 19, 'imagenes/producto/s-works epic 29.png'),
(25, 26, 'imagenes/producto/Suplemento.png'),
(26, 27, 'imagenes/producto/Gel energetico.jpg'),
(28, 25, 'imagenes/producto/Licras.png'),
(29, 24, 'imagenes/producto/Camiseta.png'),
(30, 23, 'imagenes/producto/Guantes.jpg'),
(31, 22, 'imagenes/producto/Casco.png'),
(33, 20, 'imagenes/producto/Llanta Specialized.png'),
(34, 29, 'imagenes/producto/Ruedas.jpg'),
(35, 30, 'imagenes/producto/Mavic.png'),
(36, 31, 'imagenes/producto/Gafas Scott.png'),
(37, 32, 'imagenes/producto/Licra mujer.png'),
(38, 33, 'imagenes/producto/power gel.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE IF NOT EXISTS `marca` (
  `id_mk` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `nam_mk` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_mk`),
  KEY `tipo_id` (`tipo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_mk`, `tipo_id`, `nam_mk`) VALUES
(6, 6, 'Specialized'),
(7, 6, 'Giant'),
(8, 6, 'Scott'),
(9, 6, 'Focus'),
(10, 6, 'Otros'),
(11, 7, 'Specialized'),
(12, 7, 'Shimano'),
(13, 7, 'Sram'),
(14, 7, 'Mavic'),
(15, 7, 'Otros'),
(20, 8, 'Specialized'),
(21, 8, 'Giant'),
(22, 8, 'Scott'),
(23, 8, 'Spiuk'),
(24, 8, 'Otros'),
(25, 9, 'Specialized'),
(26, 9, 'Suarez'),
(27, 9, 'Zerie'),
(28, 9, 'Torralba'),
(29, 9, 'L. Garneau'),
(30, 9, 'Otros'),
(32, 10, 'Firts Endurance'),
(33, 10, 'Actimax'),
(34, 10, 'NSF'),
(35, 10, 'Vega'),
(36, 10, 'Power Gel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `nam_pais` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=241 ;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `nam_pais`) VALUES
(1, 'Afganistan'),
(2, 'Islas Gland'),
(3, 'Albania'),
(4, 'Alemania'),
(5, 'Andorra'),
(6, 'Angola'),
(7, 'Anguilla'),
(8, 'Antartida'),
(9, 'Antigua y Barbuda'),
(10, 'Antillas Holandesas'),
(11, 'Arabia Saudi'),
(12, 'Argelia'),
(13, 'Argentina'),
(14, 'Armenia'),
(15, 'Aruba'),
(16, 'Australia'),
(17, 'Austria'),
(18, 'Azerbaiyan'),
(19, 'Bahamas'),
(20, 'Bahrein'),
(21, 'Bangladesh'),
(22, 'Barbados'),
(23, 'Bielorrusia'),
(24, 'Belgica'),
(25, 'Belice'),
(26, 'Benin'),
(27, 'Bermudas'),
(28, 'Bhutan'),
(29, 'Bolivia'),
(30, 'Bosnia y Herzegovina'),
(31, 'Botsuana'),
(32, 'Isla Bouvet'),
(33, 'Brasil'),
(34, 'Brunei'),
(35, 'Bulgaria'),
(36, 'Burkina Faso'),
(37, 'Burundi'),
(38, 'Cabo Verde'),
(39, 'Islas Caiman'),
(40, 'Camboya'),
(41, 'Camerun'),
(42, 'Canada'),
(43, 'Republica Centroafricana'),
(44, 'Chad'),
(45, 'Republica Checa'),
(46, 'Chile'),
(47, 'China'),
(48, 'Chipre'),
(49, 'Isla de Navidad'),
(50, 'Ciudad del Vaticano'),
(51, 'Islas Cocos'),
(52, 'Colombia'),
(53, 'Comoras'),
(54, 'Republica Democratica del Congo'),
(55, 'Congo'),
(56, 'Islas Cook'),
(57, 'Corea del Norte'),
(58, 'Corea del Sur'),
(59, 'Costa de Marfil'),
(60, 'Costa Rica'),
(61, 'Croacia'),
(62, 'Cuba'),
(63, 'Dinamarca'),
(64, 'Dominica'),
(65, 'Republica Dominicana'),
(66, 'Ecuador'),
(67, 'Egipto'),
(68, 'El Salvador'),
(69, 'Emiratos Arabes Unidos'),
(70, 'Eritrea'),
(71, 'Eslovaquia'),
(72, 'Eslovenia'),
(73, 'España'),
(74, 'Islas ultramarinas de Estados Unidos'),
(75, 'Estados Unidos'),
(76, 'Estonia'),
(77, 'Etiopia'),
(78, 'Islas Feroe'),
(79, 'Filipinas'),
(80, 'Finlandia'),
(81, 'Fiyi'),
(82, 'Francia'),
(83, 'Gabon'),
(84, 'Gambia'),
(85, 'Georgia'),
(86, 'Islas Georgias del Sur y Sandwich del Sur'),
(87, 'Ghana'),
(88, 'Gibraltar'),
(89, 'Granada'),
(90, 'Grecia'),
(91, 'Groenlandia'),
(92, 'Guadalupe'),
(93, 'Guam'),
(94, 'Guatemala'),
(95, 'Guayana Francesa'),
(96, 'Guinea'),
(97, 'Guinea Ecuatorial'),
(98, 'Guinea-Bissau'),
(99, 'Guyana'),
(100, 'Haiti'),
(101, 'Islas Heard y McDonald'),
(102, 'Honduras'),
(103, 'Hong Kong'),
(104, 'Hungria'),
(105, 'India'),
(106, 'Indonesia'),
(107, 'Iran'),
(108, 'Iraq'),
(109, 'Irlanda'),
(110, 'Islandia'),
(111, 'Israel'),
(112, 'Italia'),
(113, 'Jamaica'),
(114, 'Japon'),
(115, 'Jordania'),
(116, 'Kazajstan'),
(117, 'Kenia'),
(118, 'Kirguistan'),
(119, 'Kiribati'),
(120, 'Kuwait'),
(121, 'Laos'),
(122, 'Lesotho'),
(123, 'Letonia'),
(124, 'Líbano'),
(125, 'Liberia'),
(126, 'Libia'),
(127, 'Liechtenstein'),
(128, 'Lituania'),
(129, 'Luxemburgo'),
(130, 'Macao'),
(131, 'ARY Macedonia'),
(132, 'Madagascar'),
(133, 'Malasia'),
(134, 'Malawi'),
(135, 'Maldivas'),
(136, 'Malí'),
(137, 'Malta'),
(138, 'Islas Malvinas'),
(139, 'Islas Marianas del Norte'),
(140, 'Marruecos'),
(141, 'Islas Marshall'),
(142, 'Martinica'),
(143, 'Mauricio'),
(144, 'Mauritania'),
(145, 'Mayotte'),
(146, 'Mexico'),
(147, 'Micronesia'),
(148, 'Moldavia'),
(149, 'Monaco'),
(150, 'Mongolia'),
(151, 'Montserrat'),
(152, 'Mozambique'),
(153, 'Myanmar'),
(154, 'Namibia'),
(155, 'Nauru'),
(156, 'Nepal'),
(157, 'Nicaragua'),
(158, 'Níger'),
(159, 'Nigeria'),
(160, 'Niue'),
(161, 'Isla Norfolk'),
(162, 'Noruega'),
(163, 'Nueva Caledonia'),
(164, 'Nueva Zelanda'),
(165, 'Oman'),
(166, 'Países Bajos'),
(167, 'Pakistan'),
(168, 'Palau'),
(169, 'Palestina'),
(170, 'Panama'),
(171, 'Papua Nueva Guinea'),
(172, 'Paraguay'),
(173, 'Peru'),
(174, 'Islas Pitcairn'),
(175, 'Polinesia Francesa'),
(176, 'Polonia'),
(177, 'Portugal'),
(178, 'Puerto Rico'),
(179, 'Qatar'),
(180, 'Reino Unido'),
(181, 'Reunion'),
(182, 'Ruanda'),
(183, 'Rumania'),
(184, 'Rusia'),
(185, 'Sahara Occidental'),
(186, 'Islas Salomon'),
(187, 'Samoa'),
(188, 'Samoa Americana'),
(189, 'San Cristobal y Nevis'),
(190, 'San Marino'),
(191, 'San Pedro y Miquelon'),
(192, 'San Vicente y las Granadinas'),
(193, 'Santa Helena'),
(194, 'Santa Lucia'),
(195, 'Santo Tome y Principe'),
(196, 'Senegal'),
(197, 'Serbia y Montenegro'),
(198, 'Seychelles'),
(199, 'Sierra Leona'),
(200, 'Singapur'),
(201, 'Siria'),
(202, 'Somalia'),
(203, 'Sri Lanka'),
(204, 'Suazilandia'),
(205, 'Sudafrica'),
(206, 'Sudan'),
(207, 'Suecia'),
(208, 'Suiza'),
(209, 'Surinam'),
(210, 'Svalbard y Jan Mayen'),
(211, 'Tailandia'),
(212, 'Taiwan'),
(213, 'Tanzania'),
(214, 'Tayikistan'),
(215, 'Territorio Britanico del Oceano Indico'),
(216, 'Territorios Australes Franceses'),
(217, 'Timor Oriental'),
(218, 'Togo'),
(219, 'Tokelau'),
(220, 'Tonga'),
(221, 'Trinidad y Tobago'),
(222, 'Tunez'),
(223, 'Islas Turcas y Caicos'),
(224, 'Turkmenistan'),
(225, 'Turquia'),
(226, 'Tuvalu'),
(227, 'Ucrania'),
(228, 'Uganda'),
(229, 'Uruguay'),
(230, 'Uzbekistan'),
(231, 'Vanuatu'),
(232, 'Venezuela'),
(233, 'Vietnam'),
(234, 'Islas Virgenes Britanicas'),
(235, 'Islas Virgenes de los Estados Unidos'),
(236, 'Wallis y Futuna'),
(237, 'Yemen'),
(238, 'Yibuti'),
(239, 'Zambia'),
(240, 'Zimbabue');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `nam_producto` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cl_id` int(11) DEFAULT NULL,
  `tp_id` int(11) NOT NULL,
  `sb_id` int(11) DEFAULT NULL,
  `vsb_id` int(11) DEFAULT NULL,
  `marca_id` int(11) NOT NULL,
  `cant_p` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `precio_p` decimal(10,0) NOT NULL,
  `text_p` text COLLATE utf8_spanish_ci NOT NULL,
  `fec_p` date NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `cl_id` (`cl_id`,`tp_id`),
  KEY `tp_id` (`tp_id`),
  KEY `marca_id` (`marca_id`),
  KEY `sb_id` (`sb_id`),
  KEY `vsb_id` (`vsb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nam_producto`, `cl_id`, `tp_id`, `sb_id`, `vsb_id`, `marca_id`, `cant_p`, `precio_p`, `text_p`, `fec_p`) VALUES
(15, 'Pro bike ', 6, 6, 7, 5, 6, '', '0', '', '2015-11-13'),
(17, 'Venge 2 ', 6, 6, 7, 5, 6, '', '0', '', '2015-11-18'),
(18, 'SL 4 ', 6, 6, 7, 5, 6, '', '0', '', '2015-11-18'),
(19, 's-works epic 29', 6, 6, 7, 6, 6, '', '0', '', '2015-11-18'),
(20, 'Llanta 01', NULL, 7, 21, 12, 11, '', '0', '', '2015-11-18'),
(22, 'Casco', NULL, 8, 8, 15, 22, '', '0', '', '2015-11-18'),
(23, 'Guantes ', NULL, 8, 8, 17, 21, '', '0', '', '2015-11-18'),
(24, 'Camiseta ', 6, 9, 14, 29, 25, '', '0', '', '2015-11-19'),
(25, 'Licras', 6, 9, 13, 21, 25, '', '0', '', '2015-11-19'),
(26, 'Suplemento ', NULL, 10, 17, 28, 32, '', '0', '', '2015-11-24'),
(27, 'Gel EnergÃ©tico ', NULL, 10, 18, 27, 33, '', '0', '', '2015-11-25'),
(29, 'Ruedas ', NULL, 7, 22, 11, 12, '', '0', '', '2015-11-25'),
(30, 'SuspensiÃ³n ', NULL, 7, 22, 13, 14, '', '0', '', '2015-11-25'),
(31, 'Gafas ', NULL, 8, 10, 18, 22, '', '0', '', '2015-11-25'),
(32, 'Licra mujer', 7, 9, 14, 20, 25, '', '0', '', '2015-11-25'),
(33, 'Power Gel', NULL, 10, 18, 27, 36, '', '0', '', '2015-11-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pr_tal_rel`
--

CREATE TABLE IF NOT EXISTS `pr_tal_rel` (
  `id_rel` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `cant_rel` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_rel`),
  KEY `producto_id` (`producto_id`,`talla_id`),
  KEY `talla_id` (`talla_id`),
  KEY `color_id` (`color_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subtipos`
--

CREATE TABLE IF NOT EXISTS `subtipos` (
  `id_subtp` int(11) NOT NULL AUTO_INCREMENT,
  `tp_id` int(11) NOT NULL,
  `nm_subtp` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_subtp`),
  KEY `tp_id` (`tp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `subtipos`
--

INSERT INTO `subtipos` (`id_subtp`, `tp_id`, `nm_subtp`) VALUES
(4, 6, 'Rin 29'),
(5, 6, 'Rin 27.5'),
(6, 6, 'Rin 700'),
(7, 6, 'Otros'),
(8, 8, 'MTB'),
(9, 8, 'Ruta'),
(10, 8, 'Fitness'),
(11, 8, 'Urbanas'),
(12, 9, 'MTB'),
(13, 9, 'Ruta'),
(14, 9, 'Fitness'),
(15, 9, 'Urbanas'),
(16, 10, 'Pastillas'),
(17, 10, 'En polvo'),
(18, 10, 'En Gel'),
(19, 10, 'Liquido'),
(20, 7, 'MTB'),
(21, 7, 'Ruta'),
(22, 7, 'Fitness'),
(23, 7, 'Urbanas3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallas`
--

CREATE TABLE IF NOT EXISTS `tallas` (
  `id_tll` int(11) NOT NULL AUTO_INCREMENT,
  `tp_id` int(11) NOT NULL,
  `nam_tll` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_tll`),
  KEY `tp_id` (`tp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `tallas`
--

INSERT INTO `tallas` (`id_tll`, `tp_id`, `nam_tll`) VALUES
(12, 6, 'XXL'),
(13, 6, 'XL'),
(14, 6, 'L'),
(15, 6, 'M'),
(16, 6, 'S'),
(17, 6, 'XS'),
(18, 8, 'XXL'),
(19, 8, 'XL'),
(26, 8, 'L'),
(27, 8, 'M'),
(28, 8, 'S'),
(29, 8, 'XS'),
(30, 9, 'XXL'),
(31, 9, 'XL'),
(32, 9, 'L'),
(33, 9, 'M'),
(34, 9, 'S'),
(35, 9, 'XS'),
(38, 7, 'otros'),
(39, 7, 'Rin 700'),
(40, 7, 'Rin 27.5'),
(41, 7, 'Rin29'),
(42, 10, 'Mayor a 500gr'),
(43, 10, 'Igual a 500gr'),
(44, 10, 'Menor de 500gt');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `texto`
--

CREATE TABLE IF NOT EXISTS `texto` (
  `id_text` int(11) NOT NULL AUTO_INCREMENT,
  `tt_text` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `txt_text` text COLLATE utf8_spanish_ci NOT NULL,
  `fec_text` date NOT NULL,
  PRIMARY KEY (`id_text`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `texto`
--

INSERT INTO `texto` (`id_text`, `tt_text`, `txt_text`, `fec_text`) VALUES
(1, 'PolÃ­ticas de devoluciÃ³n ', '<p>Nuestra pol&iacute;tica de devoluci&oacute;n es muy sencilla. Podr&aacute;s devolver cualquier art&iacute;culo comprado en COYOTE STORE S.A.S por las siguientes causas en un t&eacute;rmino no mayor a ocho (8) d&iacute;as h&aacute;biles:</p>\r\n\r\n<p>Si el art&iacute;culo presenta defectos de fabricaci&oacute;n. Si existe equivocaci&oacute;n en el art&iacute;culo enviado. El producto deber&aacute; ser devuelto en las mismas condiciones en que lo recibi&oacute;. Los costos de transporte y los dem&aacute;s que conlleve a la devoluci&oacute;n ser&aacute;n cubiertos por el consumidor. De acuerdo con el&nbsp;<a href="http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=44306">derecho de retracto</a>&nbsp;consagrado por la ley 1480 art. 47, el t&eacute;rmino m&aacute;ximo para ejercer el derecho de retracto es de 5 d&iacute;as h&aacute;biles contados a partir de la entrega del bien.</p>\r\n\r\n<p>En caso de que se cumplan todas las condiciones mencionadas, COYOTE STORE S.A.S devolver&aacute; la suma total abonada, sin que se procedan a hacer descuentos o retenciones a exenci&oacute;n de los costos generados por transporte, y el t&eacute;rmino no podr&aacute; exceder de 30 d&iacute;as calendarios.</p>\r\n\r\n<p>En la recepci&oacute;n de mercanc&iacute;a err&oacute;nea o da&ntilde;ada se aplicar&aacute; el cambio f&iacute;sico de la misma solo si &eacute;sta fue reportada durante las primeras 72 horas posteriores a su entrega, al siguiente tel&eacute;fono: Tel&eacute;fono de Atenci&oacute;n a Clientes: (5) 2751065</p>\r\n', '2015-09-04'),
(2, 'PolÃ­ticas de envÃ­o', '<p>Los siguientes t&eacute;rminos aplican para todos los env&iacute;os realizados dentro de la Rep&uacute;blica de Colombia.</p>\r\n\r\n<p>Los precios publicados en el sitio NO incluyen costo de env&iacute;o dentro de la Rep&uacute;blica de Colombiana salvo cuando expresamente lo indique alg&uacute;n producto o alguna promoci&oacute;n en particular, los precios de los productos pueden cambiar en cualquier momento sin previo aviso, no se pueden combinar promociones de otros medios distintos a los presentados en este sitio.</p>\r\n\r\n<p>El cliente se compromete a proporcionar una direcci&oacute;n v&aacute;lida localizable dentro de la rep&uacute;blica de Colombia donde pueda entreg&aacute;rsele el pedido. En caso de que el cliente proporcione una direcci&oacute;n que no sea localizada por la transportadora, el costo adicional ser&aacute; cubierto por el cliente.</p>\r\n\r\n<p>Todos los env&iacute;os realizados por nosotros y por nuestros proveedores van perfectamente empacados, en caso de que su pedido le sea entregado con da&ntilde;o f&iacute;sico por parte de la transportadora, usted debe de agregar una nota al firmar el manifiesto al empleado de la transportadora indicando que el paquete presenta se&ntilde;ales de maltrato (Caja abierta, golpeada, perforada, mojada, etc.) Sin esta anotaci&oacute;n usted recibe de conformidad el paquete. Si el empleado de la transportadora no le permite anotar en el manifiesto de entrega usted NO deber&aacute; de recibir el paquete.</p>\r\n\r\n<p>Si usted sospecha que la caja que le est&aacute; entregando la transportadora no contiene lo que usted compr&oacute; ya sea por el peso o tama&ntilde;o del empaque NO deber&aacute; de recibirlo indicando al empleado de la transportadora los motivos y deber&aacute; de reportarlo con nosotros inmediatamente para hacer la aclaraci&oacute;n de su pedido.</p>\r\n\r\n<p>Una vez recibido el paquete por usted o alguna persona en la direcci&oacute;n de entrega, cuenta con 24 horas para reportar anomal&iacute;as por da&ntilde;o f&iacute;sico o faltante en su pedido, despu&eacute;s de este plazo no procede ning&uacute;n cambio o reclamaci&oacute;n.</p>\r\n\r\n<p>&Aacute;rea de reparto</p>\r\n\r\n<p>El &aacute;rea de reparto de nuestros productos se limita a los departamentos de la Republica de Colombia. Entregamos en todas las localidades donde existe cobertura a trav&eacute;s de las empresas transportadoras con las que tenemos servicio. En las entregas por transportadoras dependemos totalmente de las condiciones de cada una de ellas, as&iacute; mismos de las condiciones climatol&oacute;gicas y zonas de riesgo de cada entidad.</p>\r\n\r\n<p>No podemos procesar pedidos con la direcci&oacute;n de entrega incompleta, ni con convenios especiales de otras transportadoras.</p>\r\n\r\n<p>IMPORTANTE:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>En los pedidos que salen desde el almac&eacute;n de Sincelejo, los tiempos de entrega ser&aacute;n los normales y depender&aacute; &uacute;nica y exclusivamente de la ciudad de destino y el tiempo que la transportadora demore en entregarlo.</p>\r\n	</li>\r\n	<li>\r\n	<p>Si su pedido contiene productos con existencia en distintos almacenes se le cobrar&aacute; por cada uno de los env&iacute;os.</p>\r\n	</li>\r\n	<li>\r\n	<p>Si su pedido incluye productos sobre pedido, estos tomaran un poco m&aacute;s de tiempo en ser entregados.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>TIEMPO DE ENTREGA</strong></p>\r\n\r\n<p>El tiempo de entrega del producto comienza al momento de Liberarse el pedido</p>\r\n\r\n<p>El tiempo estimado de entrega le ser&aacute; proporcionado al concluir su pedido y ser&aacute; confirmado a trav&eacute;s de correo electr&oacute;nico y/o v&iacute;a telef&oacute;nica.</p>\r\n\r\n<p>Tiempo de Entrega de Productos en existencia depende de la liberaci&oacute;n de su pedido seg&uacute;n el m&eacute;todo de pago.</p>\r\n\r\n<p>Tiempo de Entrega de Productos Sobre Pedido sobre pedido depender&aacute; de la disponibilidad del mismo, COYOTE STORE S.A.S se mantendr&aacute; en comunicaci&oacute;n con el proveedor y con el cliente, sin embargo, por pol&iacute;ticas de estos proveedores ellos pueden decidir:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>No surtir alg&uacute;n producto por sus pol&iacute;ticas comerciales o cualquier otra circunstancia ajena a nuestra empresa, en este caso se le notificar&aacute; por correo electr&oacute;nico para que usted decida cambiar su producto por otro modelo o cancelar su pedido.</p>\r\n	</li>\r\n	<li>\r\n	<p>Surtir el pedido en un mayor tiempo al estimado, en este caso se le notificara por correo electr&oacute;nico para que usted decida si desea seguir en espera o cancelar su pedido. Los pedidos pagados con m&aacute;s de cuatro (4) semanas cuentan con garant&iacute;a de reembolso, lo que significa que usted podr&aacute; solicitar la devoluci&oacute;n de lo pagado.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Si su pedido incluye varios productos sobre pedido, se enviar&aacute; al momento de estar todo su pedido completo, si usted necesita que se env&iacute;en por separado se le cobrar&aacute; seg&uacute;n tarifas descritas a continuaci&oacute;n un env&iacute;o cada vez que se libere alg&uacute;n producto del almac&eacute;n.</p>\r\n\r\n<p><strong>TARIFAS DE ENVIO</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>&iquest;Cu&aacute;nto cuesta el env&iacute;o de los productos?</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">Los env&iacute;os a nivel nacional tienen los siguientes costos:</p>\r\n\r\n<p style="margin-left:1.27cm">Compras superiores a $ 150.000:&nbsp;ENVIO TOTALMENTE GRATIS</p>\r\n\r\n<p style="margin-left:1.27cm">Compras inferiores a $ 98.999 tienen un costo de $ 9.900</p>\r\n\r\n<p style="margin-left:1.27cm">Compras entre $ 99.000 - $ 149.999 tienen un costo de $ 5.900</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>&nbsp;&iquest;Cu&aacute;l es la empresa responsable del env&iacute;o de los productos?</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">&nbsp;Las empresas responsables de realizar los env&iacute;os de COYOTE STORE S.A.S son Saferbo y Coordinadora Mercantil.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>&iquest;C&oacute;mo puedo hacer un seguimiento de mi pedido?</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">Una vez enviado el producto te enviamos el n&uacute;mero de gu&iacute;a al correo electr&oacute;nico que registraste en el momento de la compra.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Plazo de Entrega</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">El plazo de entrega est&aacute; entre 2 a 7 d&iacute;as h&aacute;biles. El plazo de entrega comenzar&aacute; a contar a partir de la confirmaci&oacute;n de tu pago por parte de COYOTE STORE S.A.S y el an&aacute;lisis de los datos, lo cual puede tardar un d&iacute;a h&aacute;bil.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Entregas internacionales.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">COYOTE STORE S.A.S No realiza entregas por fuera de la Republica de Colombia.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>No recib&iacute; mi pedido &iquest;Qu&eacute; puedo hacer si no recib&iacute; mi pedido?</p>\r\n	</li>\r\n</ul>\r\n\r\n<p style="margin-left:1.27cm">Si no recibiste tu pedido en el plazo estipulado, te pedimos que por favor te contactes con nuestro Centro de Atenci&oacute;n al Cliente. Verificaremos junto a la empresa responsable de tu env&iacute;o lo ocurrido y nos comunicaremos contigo para darte una respuesta tan pronto sea posible.</p>\r\n', '2015-09-04'),
(3, 'Formas de pago ', '<p><strong>PAGOS CON DEPOSITO O TRANSFERENCIA ELECTRONICA</strong></p>\r\n\r\n<p>En esta modalidad los productos que se encuentran en existencia se env&iacute;an generalmente en 24 horas h&aacute;biles (de Lunes a Viernes) una vez comprobado su dep&oacute;sito y liberado su pedido.</p>\r\n\r\n<p>Para agilizar la liberaci&oacute;n es importante que notifique su pago</p>\r\n\r\n<p>Por pagina web: http://www.??????????????????.com/contacto</p>\r\n\r\n<p>Por correo:&nbsp;pagos@???????????.com</p>\r\n\r\n<p>Por tel&eacute;fono Sincelejo &ndash; Sucre: (5) 275-1065<br />\r\n<br />\r\n&nbsp;</p>\r\n\r\n<p>Por WhatsApp : 301 &ndash; 337 0933</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>Es necesario que env&iacute;e el comprobante de su dep&oacute;sito o transferencia, en caso de una aclaraci&oacute;n un Ejecutivos de ventas se comunicara con usted para solicitar m&aacute;s informaci&oacute;n de su pago.</p>\r\n\r\n<p>Favor de tomar en cuenta que si su dep&oacute;sito es con cheque de otro banco o de otra plaza, el pago se acredita hasta cuatro d&iacute;as (4) h&aacute;biles siguientes, una vez acreditado se procede a liberar su pedido.</p>\r\n\r\n<p>Para su comodidad ponemos a su disposici&oacute;n nuestra cuenta Bancaria en:&nbsp;Bancolombia</p>\r\n\r\n<p>Esta aparecer&aacute; una vez llenada la informaci&oacute;n de Registro</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><strong>PAGOS CON TARJETA CREDITO</strong></p>\r\n\r\n<p>En esta modalidad los productos se env&iacute;an una vez que su banco emisor y el departamento de seguridad del banco que hace la cobranza libere su pago. Esto generalmente es al d&iacute;a h&aacute;bil siguiente despu&eacute;s de su pago. COYOTE STORE S.A.S se reserva el derecho de cancelar cualquier pago que no cumpla con los requisitos que solicite el banco, esto significa que en ciertos casos se le puede pedir que nos env&iacute;e una copia de su identificaci&oacute;n y/o comprobante de domicilio y otros documentos que comprueben su identidad &uacute;nicamente con el fin de verificarla.</p>\r\n\r\n<p>Para los pagos con tarjeta de cr&eacute;dito, COYOTE STORE S.A.S utiliza los servicios de Bancolombia para el cobro y validaci&oacute;n de la informaci&oacute;n del cliente. COYOTE STORE S.A.S solo remite la informaci&oacute;n del cliente con el tercero (en este caso Bncolombia y sus afiliados) para que procesen el pago realizado y en su caso notifiquen a COYOTE STORE S.A.S la autorizaci&oacute;n de la transacci&oacute;n. COYOTE STORE S.A.S estipula en sus T&eacute;rminos y Condiciones que el cliente acepta que la informaci&oacute;n ser&aacute; remitida a un tercero para su validaci&oacute;n y autorizaci&oacute;n.</p>\r\n\r\n<p><a href="">http://www.??????????????.com/privacidad</a></p>\r\n\r\n<p>En caso de cualquier falta de documentos solicitado su pago ser&aacute; revertido.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><strong>PAGOS CON SISTEMAS DE GIROS NACIONALES</strong></p>\r\n\r\n<p>En esta modalidad los productos se env&iacute;an una vez sea verificado en Giro en cualquiera de las agencias operadoras de este tipo de servicios a nivel nacional y que tengan sede en la ciudad de Sincelejo (Sucre).</p>\r\n', '2015-09-04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE IF NOT EXISTS `tipo_producto` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nam_tipo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ll_cl` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id_tipo`, `nam_tipo`, `ll_cl`) VALUES
(6, 'Bicicletas', '1'),
(7, 'Componentes', '2'),
(8, 'Accesorios', '1'),
(9, 'Ropa', '1'),
(10, 'Suplementos deportivos', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_us` int(11) NOT NULL AUTO_INCREMENT,
  `avat_us` varchar(450) COLLATE utf8_spanish_ci NOT NULL,
  `nam_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ape_us` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `correo_us` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `password_us` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_us` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `celular_us` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `ciudad_id` int(11) DEFAULT NULL,
  `direccion_us` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `tip_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `estd_us` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `cod_us` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `corcab_us` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_us`),
  KEY `pais_id` (`pais_id`,`ciudad_id`),
  KEY `ciudad_id` (`ciudad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_us`, `avat_us`, `nam_us`, `ape_us`, `correo_us`, `password_us`, `telefono_us`, `celular_us`, `pais_id`, `ciudad_id`, `direccion_us`, `tip_us`, `estd_us`, `cod_us`, `corcab_us`) VALUES
(1, 'imagenes/avatar/fondo.jpg', 'albert', 'arias', 'albertarias925@outlook.com', 'dragon', '5768508', '3118690175', 52, 22, 'cll 15 #2-45 mot', '', '1', '000', ''),
(2, '', 'Pepe', 'Pepon', 'correostodobasura@gmail.com', 'Pepon00', '', '', NULL, NULL, '', '1', '1', '000', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vsb_g`
--

CREATE TABLE IF NOT EXISTS `vsb_g` (
  `id_vsb` int(11) NOT NULL AUTO_INCREMENT,
  `tp_id` int(11) NOT NULL,
  `nam_vsb` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_vsb`),
  KEY `tp_id` (`tp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `vsb_g`
--

INSERT INTO `vsb_g` (`id_vsb`, `tp_id`, `nam_vsb`) VALUES
(5, 6, 'Ruta'),
(6, 6, 'MTB'),
(7, 6, 'Urbanas'),
(8, 6, 'Fitness'),
(9, 6, 'Usadas'),
(10, 7, 'Partes'),
(11, 7, 'Ruedas'),
(12, 7, 'Llantas'),
(13, 7, 'Suspensiones'),
(14, 7, 'Otros'),
(15, 8, 'Cascos'),
(16, 8, 'Zapatillas'),
(17, 8, 'Guantes'),
(18, 8, 'Gafas'),
(19, 8, 'Otros'),
(20, 9, 'Licras'),
(21, 9, 'Pantalones'),
(22, 9, 'Medias'),
(23, 9, 'Otros'),
(24, 10, 'Hidratantes'),
(25, 10, 'Recuperantes'),
(26, 10, 'Vitaminas'),
(27, 10, 'Geles'),
(28, 10, 'Fitness2'),
(29, 9, 'Camisetas');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_us`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `galery`
--
ALTER TABLE `galery`
  ADD CONSTRAINT `galery_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `img_pr`
--
ALTER TABLE `img_pr`
  ADD CONSTRAINT `img_pr_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `marca_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_producto` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`cl_id`) REFERENCES `cliente` (`id_cl`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`tp_id`) REFERENCES `tipo_producto` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_3` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id_mk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_4` FOREIGN KEY (`sb_id`) REFERENCES `subtipos` (`id_subtp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_5` FOREIGN KEY (`vsb_id`) REFERENCES `vsb_g` (`id_vsb`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pr_tal_rel`
--
ALTER TABLE `pr_tal_rel`
  ADD CONSTRAINT `pr_tal_rel_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pr_tal_rel_ibfk_2` FOREIGN KEY (`talla_id`) REFERENCES `tallas` (`id_tll`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pr_tal_rel_ibfk_3` FOREIGN KEY (`color_id`) REFERENCES `colores` (`id_color`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `subtipos`
--
ALTER TABLE `subtipos`
  ADD CONSTRAINT `subtipos_ibfk_1` FOREIGN KEY (`tp_id`) REFERENCES `tipo_producto` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tallas`
--
ALTER TABLE `tallas`
  ADD CONSTRAINT `tallas_ibfk_1` FOREIGN KEY (`tp_id`) REFERENCES `tipo_producto` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudad` (`id_ciudad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vsb_g`
--
ALTER TABLE `vsb_g`
  ADD CONSTRAINT `vsb_g_ibfk_1` FOREIGN KEY (`tp_id`) REFERENCES `tipo_producto` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
