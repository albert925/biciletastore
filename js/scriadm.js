$(document).on("ready",iniadminscrpag);
function iniadminscrpag () {
	$("#nva").on("click",abrirA);
	$("#nvb").on("click",abrirB);
	$("#nvc").on("click",abrirC);
	$("#cptp").on("change",buscarsubtipo);
	$("#cptp").on("change",buscarsubsubusubtp);
	$("#cptp").on("change",buscarsubtiposdos);
	$(".dell").on("click",comfirmarborrar);
	$(".cerrar").on("click",cerrV);
}
function comfirmarborrar () {
	return confirm("¿Estas seguro de eliminar el dato?");
}
function abrirA (ak) {
	ak.preventDefault();
	$("#caj").each(animarA);
}
function abrirB (bk) {
	bk.preventDefault();
	$("#cbj").each(animarB);
}
function abrirC (ck) {
	ck.preventDefault();
	$("#ccj").each(animarC);
}
function animarA () {
	var altoA=$(this).css("height");
	if (altoA=="150px") {
		$(this).animate({height:"0"}, 500);
	}
	else{
		$(this).animate({height:"150px"}, 500);
	}
}
function animarB () {
	var altoA=$(this).css("height");
	if (altoA=="850px") {
		$(this).animate({height:"0"}, 500);
	}
	else{
		$(this).animate({height:"950px"}, 500);
	}
}
function animarC () {
	var altoA=$(this).css("height");
	if (altoA=="550px") {
		$(this).animate({height:"0"}, 500);
	}
	else{
		$(this).animate({height:"550px"}, 500);
	}
}
function buscarsubtipo () {
	var tipB=$("#cptp").val();
	$.post("busq_submark.php",{sytp:tipB},colocarmarca);
}
function colocarmarca (dmkd) {
	$("#dpmk").html(dmkd);
}
function buscarsubsubusubtp () {
	var tipC=$("#cptp").val();
	$.post("busq_subsubT.php",{satp:tipC},colosobsubsobsub);
}
function colosobsubsobsub (dmdsbsb) {
	$("#usbsby").html(dmdsbsb);
}
function buscarsubtiposdos () {
	var tipD=$("#cptp").val();
	$.post("busq_vvtp.php",{ka:tipD},colocarsubsubvv);
}
function colocarsubsubvv (vvRvv) {
	$("#vvvsby").html(vvRvv);
}
function cerrV () {
	$(".datusF").fadeOut();
}
